package campusdigitalwebtest.utils;

import java.io.File;

public class Utils {

    public static String getDownloadsFolder() {
        String userprofile = System.getenv("USERPROFILE");
        userprofile += "\\Downloads";
        return userprofile;
    }

    public static File getDownloadsFileFolder() {
        File folder = new File(getDownloadsFolder());
        return folder.isDirectory() ? folder : null;
    }

    public static boolean fileExists(String path) {
        File tmpFile = new File(path);
        return tmpFile.exists() && tmpFile.isFile();
    }

}
