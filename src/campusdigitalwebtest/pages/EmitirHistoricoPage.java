package campusdigitalwebtest.pages;

import campusdigitalwebtest.utils.Utils;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class EmitirHistoricoPage extends HomePage {

    static String panelTitleCssSelector = "span.ui-panel-title";

    public EmitirHistoricoPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public boolean isValid() {
        try {
            WebElement panelTitle = this.getPanelTitleElement();
            return panelTitle != null && super.isValid() && panelTitle.getText().contains("Histórico para simples conferência");
        } catch (StaleElementReferenceException e) {
            return false;
        }
    }

    private WebElement getPanelTitleElement() {
        try {
            if (!this.elementExists(By.id("mainForm:emissaoRelatorioPanel"))) {
                return null;
            }
            WebElement emissaoRelatorioPanel = this.findElementById("mainForm:emissaoRelatorioPanel");
            return emissaoRelatorioPanel.findElement(By.cssSelector(panelTitleCssSelector));
        } catch (NoSuchElementException e) {
            return null;
        }
    }

    protected WebElement getBotaoEmitirHistorico() {
        WebElement mainPanel = this.findElementById("mainForm:mainPanel");
        return mainPanel.findElement(By.cssSelector("button[type=submit]"));
    }

    /**
     * historico-2018.11.20-13.43.08.pdf
     *
     * @return
     */
    protected String getHistoricoFilename() {
        Calendar cal = Calendar.getInstance();
        Date date = cal.getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd");
        return "historico-" + dateFormat.format(date);
    }

    public boolean arquivoHistoricoExists() {
        File downloads = Utils.getDownloadsFileFolder();
        String fileName = this.getHistoricoFilename();
        for (File file : downloads.listFiles()) {
            if (file.isFile() && file.getName().startsWith(fileName)) {
                return true;
            }
        }
        return false;
    }

    public void emitirHistorico() throws InterruptedException {
        WebElement button = this.getBotaoEmitirHistorico();
        button.click();
        Thread.sleep(5 * 1000);
    }

}
