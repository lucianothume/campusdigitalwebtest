package campusdigitalwebtest.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class BasePage implements IPage {

    protected WebDriver driver;

    protected BasePage(WebDriver driver) {
        this.driver = driver;
    }

    @Override
    public abstract boolean isValid();

    protected WebElement findElementById(String id) {
        try {
            return this.driver.findElement(By.id(id));
        } catch (NoSuchElementException e) {
            System.err.println("Elemento nao encontrado -> id:" + id);
            return null;
        }
    }

    protected WebElement findElementByCssSelector(String cssSelector) {
        try {
            return this.driver.findElement(By.cssSelector(cssSelector));
        } catch (NoSuchElementException e) {
            System.err.println("Elemento nao encontrado -> cssSelector:" + cssSelector);
            return null;
        }
    }

    protected boolean elementExists(By by) {
        try {
            return this.driver.findElement(by) != null;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    @Override
    public void waitLoad(long timeOutInSeconds) {
        try {
            WebDriverWait wait = new WebDriverWait(this.driver, timeOutInSeconds);
            wait.until((WebDriver driver) -> {
                return isValid();
            });
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

}
