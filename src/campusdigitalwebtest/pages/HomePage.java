package campusdigitalwebtest.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HomePage extends BasePage {

    static String mainMenuCssSelector = "#mainHorizontalMenu ul > li > a";
    static String logoutMenuCssSelector = "#logoutMenu > a";
    static String nomeUsuarioCssSelector = "#accountPreferencesMenu > div.bright_blue.bold";
    static String menuRequisitarComprovanteMatriculaCssSelector = "#mainHorizontalMenu ul > li > ul > table ul:nth-child(1) > li:nth-child(2) > a";
    static String menuConsultarHorariosCssSelector = "#mainHorizontalMenu ul > li > ul > table ul:nth-child(2) > li:nth-child(2) > a";
    static String menuConsultarAvaliacoesFrequenciaCssSelector = "#mainHorizontalMenu ul > li > ul > table ul:nth-child(3) > li:nth-child(2) > a";
    static String menuEmitirHistoricoCssSelector = "#mainHorizontalMenu ul > li > ul > table ul:nth-child(4) > li:nth-child(2) > a";

    public HomePage(WebDriver driver) {
        super(driver);
    }

    @Override
    public boolean isValid() {
        return this.elementExists(By.cssSelector(mainMenuCssSelector));
    }

    public LoginPage doLogout() {
        WebElement logoutMenu = this.findElementByCssSelector(logoutMenuCssSelector);
        logoutMenu.click();
        LoginPage loginPage = new LoginPage(this.driver);
        loginPage.waitLoad(5);
        return loginPage;
    }

    public RequisitarComprovanteMatriculaPage goRequisitarComprovanteMatricula() {
        this.openMainMenu();

        WebElement menuRequisitarComprovanteMatricula = this.findElementByCssSelector(menuRequisitarComprovanteMatriculaCssSelector);
        menuRequisitarComprovanteMatricula.click();

        RequisitarComprovanteMatriculaPage page = new RequisitarComprovanteMatriculaPage(this.driver);
        page.waitLoad(5);
        return page;
    }

    public ConsultaHorariosPage goConsultarHorarios() {
        this.openMainMenu();

        WebElement menuConsultarHorarios = this.findElementByCssSelector(menuConsultarHorariosCssSelector);
        menuConsultarHorarios.click();

        ConsultaHorariosPage page = new ConsultaHorariosPage(this.driver);
        page.waitLoad(5);
        return page;
    }

    public ConsultarAvaliacoesFrequenciaPage goConsultarAvaliacoesFrequencia() {
        this.openMainMenu();

        WebElement menuConsultarAvaliacoesFrequencia = this.findElementByCssSelector(menuConsultarAvaliacoesFrequenciaCssSelector);
        menuConsultarAvaliacoesFrequencia.click();

        ConsultarAvaliacoesFrequenciaPage page = new ConsultarAvaliacoesFrequenciaPage(this.driver);
        page.waitLoad(5);
        return page;
    }

    public EmitirHistoricoPage goEmitirHistorico() {
        this.openMainMenu();

        WebElement menuEmitirHistorico = this.findElementByCssSelector(menuEmitirHistoricoCssSelector);
        menuEmitirHistorico.click();

        EmitirHistoricoPage page = new EmitirHistoricoPage(this.driver);
        page.waitLoad(5);
        return page;
    }

    public void openMainMenu() {
        WebElement mainMenu = this.findElementByCssSelector(mainMenuCssSelector);
        if (mainMenu != null) {
            mainMenu.click();
        }
    }

}
