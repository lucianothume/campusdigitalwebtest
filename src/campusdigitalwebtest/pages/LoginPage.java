package campusdigitalwebtest.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage extends BasePage {

    static String inputUserId = "authentication:username";
    static String inputPswdId = "authentication:password";

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public boolean isValid() {
        try {
            boolean inputUserExists = this.elementExists(By.id(inputUserId));
            boolean inputPswdExists = this.elementExists(By.id(inputPswdId));
            return inputUserExists && inputPswdExists;
        } catch (Exception e) {
            return false;
        }
    }

    public HomePage doLogin(String username, String password) {
        WebElement inputUser = this.findElementById(inputUserId);
        inputUser.click();
        inputUser.clear();
        inputUser.sendKeys(username);

        WebElement inputPswd = this.findElementById(inputPswdId);
        inputPswd.click();
        inputPswd.clear();
        inputPswd.sendKeys(password);
        inputPswd.sendKeys(Keys.ENTER);

        HomePage homePage = new HomePage(this.driver);
        homePage.waitLoad(2);
        return homePage;
    }

}
