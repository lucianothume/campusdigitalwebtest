package campusdigitalwebtest.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.StaleElementReferenceException;

public class ConsultaHorariosPage extends HomePage {

    static String panelTitleCssSelector = "#mainContentPanel span.ui-panel-title";

    public ConsultaHorariosPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public boolean isValid() {
        if (!this.elementExists(By.cssSelector(panelTitleCssSelector))) {
            return false;
        }
        try {
            WebElement panelTitle = this.findElementByCssSelector(panelTitleCssSelector);
            return super.isValid() && panelTitle.getText().contains("Horários de Aulas");
        } catch (StaleElementReferenceException e) {
            return false;
        }
    }

}
