package campusdigitalwebtest.pages;

import campusdigitalwebtest.utils.Utils;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class RequisitarComprovanteMatriculaPage extends HomePage {

    static String panelTitleCssSelector = "#mainForm span.ui-panel-title";

    public RequisitarComprovanteMatriculaPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public boolean isValid() {
        if (!this.elementExists(By.cssSelector(panelTitleCssSelector))) {
            return false;
        }
        try {
            WebElement panelTitle = this.findElementByCssSelector(panelTitleCssSelector);
            return super.isValid() && panelTitle.getText().contains("Rematrícula e Comprovante");
        } catch (StaleElementReferenceException e) {
            return false;
        }
    }

    protected WebElement getBotaoEmitirComprovante() {
        WebElement mainPanel = this.findElementById("mainForm:mainPanel");
        return mainPanel.findElement(By.cssSelector("button[type=submit]"));
    }

    /**
     * comprovanteMatricula-2018.11.20-13.43.08.pdf
     *
     * @return
     */
    protected String getComprovateFilename() {
        Calendar cal = Calendar.getInstance();
        Date date = cal.getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd");
        return "comprovanteMatricula-" + dateFormat.format(date);
    }

    public void emitirComprovante() throws InterruptedException {
        WebElement button = this.getBotaoEmitirComprovante();
        button.click();
        Thread.sleep(5 * 1000);
    }

    public boolean arquivoComprovanteExists() {
        File downloads = Utils.getDownloadsFileFolder();
        String fileName = this.getComprovateFilename();
        for (File file : downloads.listFiles()) {
            if (file.isFile() && file.getName().startsWith(fileName)) {
                return true;
            }
        }
        return false;
    }

}
