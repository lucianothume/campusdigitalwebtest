package campusdigitalwebtest.pages;

public interface IPage {

    boolean isValid();

    void waitLoad(long timeOutInSeconds);

}
