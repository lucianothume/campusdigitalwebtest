package campusdigitalwebtest.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ConsultarAvaliacoesFrequenciaPage extends HomePage {

    static String panelTitleCssSelector = "#mainContentPanel span.ui-panel-title";

    public ConsultarAvaliacoesFrequenciaPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public boolean isValid() {
        if (!this.elementExists(By.cssSelector(panelTitleCssSelector))) {
            return false;
        }
        try {
            WebElement panelTitle = this.findElementByCssSelector(panelTitleCssSelector);
            return super.isValid() && panelTitle.getText().contains("Avaliações e Frequência");
        } catch (StaleElementReferenceException e) {
            return false;
        }
    }

}
