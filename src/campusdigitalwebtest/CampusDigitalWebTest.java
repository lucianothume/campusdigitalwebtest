package campusdigitalwebtest;

import campusdigitalwebtest.pages.ConsultaHorariosPage;
import campusdigitalwebtest.pages.ConsultarAvaliacoesFrequenciaPage;
import campusdigitalwebtest.pages.EmitirHistoricoPage;
import campusdigitalwebtest.pages.HomePage;
import campusdigitalwebtest.pages.LoginPage;
import campusdigitalwebtest.pages.RequisitarComprovanteMatriculaPage;
import campusdigitalwebtest.utils.Teclado;
import java.util.function.Consumer;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class CampusDigitalWebTest {

    static String matricula = "0191167";
    static String senha = "";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");

        leEntradas();
        runTest(CampusDigitalWebTest::testeLogin);
        runTest(CampusDigitalWebTest::testeLoginNegativo);
        runTest(CampusDigitalWebTest::testeEmissaoComprovante);
        runTest(CampusDigitalWebTest::testeEmissaoHistorico);
        runTest(CampusDigitalWebTest::testeConsultaHorario);

    }

    private static void runTest(Consumer<WebDriver> test) {
        WebDriver driver = new ChromeDriver();
        try {
            driver.manage().window().maximize();
            test.accept(driver);
        } finally {
            driver.close();
            driver.quit();
        }
    }

    private static void leEntradas() {
        try {
            System.out.print("Informe a matricula: ");
            matricula = Teclado.leString();

            System.out.print("Informe a senha: ");
            senha = Teclado.leSenha();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    private static void acessarCampus(WebDriver driver) {
        String urlCampusDigital = "http://campusdigital.poa.ifrs.edu.br:8080";
        driver.navigate().to(urlCampusDigital);
    }

    public static void testeLogin(WebDriver driver) {
        acessarCampus(driver);
        LoginPage loginPage = new LoginPage(driver);
        loginPage.waitLoad(5);
        HomePage homePage = loginPage.doLogin(matricula, senha);
        homePage.waitLoad(5);
        if (homePage.isValid()) {
            System.out.println("Login com sucesso");
            LoginPage logoutPage = homePage.doLogout();
            logoutPage.waitLoad(5);
        } else {
            System.err.println("Erro ao realizar Login");
        }
    }

    public static void testeLoginNegativo(WebDriver driver) {
        acessarCampus(driver);
        LoginPage loginPage = new LoginPage(driver);
        loginPage.waitLoad(5);
        HomePage homePage = loginPage.doLogin(matricula, "blablabla");
        homePage.waitLoad(5);
        if (!homePage.isValid()) {
            System.out.println("Acesso negado, teste com sucesso");
        } else {
            System.err.println("Acesso permitido, erro no teste");
            LoginPage logoutPage = homePage.doLogout();
            logoutPage.waitLoad(5);
        }
    }

    public static void testeEmissaoComprovante(WebDriver driver) {
        acessarCampus(driver);
        LoginPage loginPage = new LoginPage(driver);
        loginPage.waitLoad(5);
        HomePage homePage = loginPage.doLogin(matricula, senha);
        homePage.waitLoad(5);
        if (!homePage.isValid()) {
            System.err.println("Erro ao realizar Login");
            return;
        }

        RequisitarComprovanteMatriculaPage reqComprovanteMatriculaPage = homePage.goRequisitarComprovanteMatricula();
        reqComprovanteMatriculaPage.waitLoad(5);
        try {
            reqComprovanteMatriculaPage.emitirComprovante();
            if (!reqComprovanteMatriculaPage.arquivoComprovanteExists()) {
                throw new InterruptedException();
            }
            System.out.println("Comprovante emitido com sucesso");
        } catch (InterruptedException ex) {
            System.err.println("Erro ao emitir comprovante");
        } finally {
            LoginPage logoutPage = homePage.doLogout();
            logoutPage.waitLoad(5);
        }
    }

    public static void testeEmissaoHistorico(WebDriver driver) {
        acessarCampus(driver);
        LoginPage loginPage = new LoginPage(driver);
        loginPage.waitLoad(5);
        HomePage homePage = loginPage.doLogin(matricula, senha);
        homePage.waitLoad(5);
        if (!homePage.isValid()) {
            System.err.println("Erro ao realizar Login");
            return;
        }

        EmitirHistoricoPage emitirHistoricoPage = homePage.goEmitirHistorico();
        emitirHistoricoPage.waitLoad(5);
        try {
            emitirHistoricoPage.emitirHistorico();
            if (!emitirHistoricoPage.arquivoHistoricoExists()) {
                throw new InterruptedException();
            }
            System.out.println("Historico emitido com sucesso");
        } catch (InterruptedException ex) {
            System.err.println("Erro ao emitir historico");
        } finally {
            LoginPage logoutPage = homePage.doLogout();
            logoutPage.waitLoad(5);
        }
    }

    public static void testeConsultaHorario(WebDriver driver) {
        acessarCampus(driver);
        LoginPage loginPage = new LoginPage(driver);
        loginPage.waitLoad(5);
        HomePage homePage = loginPage.doLogin(matricula, senha);
        homePage.waitLoad(5);
        if (!homePage.isValid()) {
            System.err.println("Erro ao realizar Login");
            return;
        }

        ConsultaHorariosPage consultaHorariosPage = homePage.goConsultarHorarios();
        consultaHorariosPage.waitLoad(5);
        System.out.println("Consulta realizada com sucesso");
        LoginPage logoutPage = consultaHorariosPage.doLogout();
        logoutPage.waitLoad(5);
    }

}
